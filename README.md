# Usage

## Usage - standard

Vous pouvez utiliser le programme comme ceci :

```
yarn start < file.txt
```

où `file.txt` contient les plages horaires indisponibles, au format `d hh:mm-hh:mm` sur chaque ligne du fichier.

## Usage - serveur web

Vous pouvez lancer un serveur web qui vous permettra de drag&drop vos fichiers.

```
yarn start -server
```

Le serveur se lancera sur `http://localhost:5000/`

# Fonctionnement

Prenons cet input :

```
1 08:45-12:59
2 08:24-10:54
1 14:45-14:47
3 09:56-16:25
5 15:16-16:28
```

Pour avoir un format plus facile à interpreter j'ai pensé à imiter le _Unix Time Stamp_ comme ceci :

```
1 08:45-12:59 -> 525-779
2 08:24-10:54 -> 885-887
1 14:45-14:47 -> 1944-2094
3 09:56-16:25 -> 3476-3865
5 15:16-16:28 -> 6676-6748
```

Chaque nombre du côté droit représente le nombre de secondes depuis Lundi minuit.
Il n'y a alors plus qu'à chercher les `X` minutes consécutives que nous voulons en respectant les horaires de travail.

`freeTimeSlot` prends comme argument _inputText_ (l'input texte de base), _duration_ (la durée du créneau à trouver) et _config_ (les horaires de travail).

# Code review

Passez en revue le code ci dessous

Si vous pensez que des modifications sont utiles

1. écrivez un commentaire comme pendant une review de pull request
2. puis écrivez le code comme vous l'imagineriez

**NB**

-   Faites ces reviews comme bon vous semble, tout n'est pas à commenter
-   Ne commentez pas le style (indentation, trailing comma, etc.)
-   Admettez que le code fonctionne
-   Ces bouts de codes fictifs n'ont rien à voir les uns avec les autres
-   Ne vous attardez pas sur des détails, comme le naming, qui ne nous intéressent pas ici

1.

```js
const data = [
    { value: "1", label: "One" },
    { value: "2", label: "Two" },
    { value: "3", label: "Three" }
];

const values = data.reduce((values, { value }) => {
    // .reduce est utilisé ici pour faire le même comportement que .map
    values.push(value);
    return values;
}, []);

// Mon code :
const values = data.map(({ value }) => value);
```

2.

```js
async function getIndexes() {
    return await fetch("https://api.coingecko.com/api/v3/indexes").then((res) =>
        res.json()
    ); // .then() est inutile dans ce cas vu que await nous donne le resultat directement.
}

// Si getIndexes() et analyzeIndexes() n'ont pas d'autres actions à faire autant le faire en une seule fonction.

async function analyzeIndexes() {
    const indexes = await getIndexes().catch((_) => {
        throw new Error("Unable to fetch indexes");
    });
    return indexes; // comme getIndexes() nous pourrions retourner directement l'appel de await getIndexes()
}

// Mon code :
async function analyzeIndexes() {
    try {
        return (await fetch("https://api.coingecko.com/api/v3/indexes")).json();
    } catch (_) {
        throw new Error("Unable to fetch indexes");
    }
}
```

3.

```js
let state; // on aurait pu déclarer state avec { user: null, project: null } ce qui nous aurait évité un else
if (user) {
    const project = getProject(user.id);
    state = {
        user,
        project
    };
} else {
    state = {
        user: null,
        project: null
    };
}
// Une syntaxe plus simple est possible
ctx.body = state;
```

Mon code:

```js
const user = getUser();
ctx.body = user
    ? { user, project: getProject(user.id) }
    : { user: null, project: null };
```

4.

```js
function getQueryProvider() {
    const url = window.location.href;
    const [_, provider] = url.match(/provider=([^&]*)/);
    if (provider) {
        return provider;
    }
    return; // !(undefined) === !(null) donc autant retourner provider directement
}
// Mon code :
function getQueryProvider() {
    const [_, provider] = window.location.href.match(/provider=([^&]*)/);
    return provider;
}
```

5.

```js
function getParagraphTexts() {
    // fonction inutile à part si la classe "NodeList" nous dérange
    const texts = [];
    document.querySelectorAll("p").forEach((p) => {
        // equivalent à Array.from()
        texts.push(p);
    });
    return texts;
}
// Mon code :
function getParagraphTexts() {
    return Array.from(document.querySelectorAll("p"));
}
```

6.

```js
function Employee({ id }) {
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [employee, setEmployee] = useState({});

    useEffect(() => {
        getEmployee(id)
            .then((employee) => {
                setEmployee(employee);
                setLoading(false);
            })
            .catch((_) => {
                setError("Unable to fetch employee");
                setLoading(false);
            });
    }, [id]);

    if (error) {
        return <Error />;
    }

    if (loading) {
        return <Loading />;
    }

    return (
        <Table>
            <Row>
                <Cell>{employee.firstName}</Cell> // répétitions et manque de
                fléxibilité
                <Cell>{employee.lastName}</Cell>
                <Cell>{employee.position}</Cell>
                <Cell>{employee.project}</Cell>
                <Cell>{employee.salary}</Cell>
                <Cell>{employee.yearHired}</Cell>
                <Cell>{employee.wololo}</Cell>
            </Row>
        </Table>
    );
}
```

Mon code :

```js
function Employee({ id }) {
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [employee, setEmployee] = useState({});
    const EmployeeInfos = ({ fields }) => {
        let list = [];
        Object.keys(employee).forEach((item, index) => {
            if (fields.includes(item))
                list.push(
                    <Cell key={"employeeCell" + index}>{employee[item]}</Cell>
                );
        });
        return <Row>{list}</Row>;
    };

    useEffect(() => {
        getEmployee(id)
            .then((employee) => {
                setEmployee(employee);
                setLoading(false);
            })
            .catch((_) => {
                setError("Unable to fetch employee");
                setLoading(false);
            });
    }, [id]);

    if (error) {
        return <Error />;
    }

    if (loading) {
        return <Loading />;
    }

    return (
        <Table>
            <EmployeeInfos
                fields={[
                    "firstName",
                    "lastName",
                    "position",
                    "project",
                    "salary",
                    "yearHired",
                    "wololo"
                ]}
            />
        </Table>
    );
}
```

7.

```js
async function getFilledIndexes() {
    try {
        const filledIndexes = [];
        const indexes = await getIndexes();
        const status = await getStatus();
        const usersId = await getUsersId();

        for (let index of indexes) {
            // .filter rendrai le code plus simple
            if (
                index.status === status.filled &&
                usersId.includes(index.userId)
            ) {
                filledIndexes.push(index);
            }
        }
        return filledIndexes;
    } catch (_) {
        throw new Error("Unable to get indexes");
    }
}

// Mon code :
async function getFilledIndexes() {
    try {
        const indexes = await getIndexes();
        const status = await getStatus();
        const usersId = await getUsersId();

        return indexes.filter(
            (index) =>
                index.status === status.filled && usersId.includes(index.userId)
        );
    } catch (_) {
        throw new Error("Unable to get indexes");
    }
}
```

8.

```js
function getUserSettings(user) {
    if (user) {
        // return directement si !user
        const project = getProject(user.id);
        if (project) {
            const settings = getSettings(project.id);
            if (settings) {
                // conditions qui auraient pu être évitées
                return settings;
            }
        }
    }
    return {};
}

// Mon code :

function getUserSettings(user) {
    if (!user) return;
    const project = getProject(user.id);
    return project ? getSettings(project.id) : false;
}
```
