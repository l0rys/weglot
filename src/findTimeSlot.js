const fs = require("fs");

const formatDate = (workDay, workMinutes, duration) => {
    // prettier-ignore
    return (
        workDay + 1 + " " + ("00" + Math.trunc(workMinutes / 60).toString()).substr(-2, 2) +
            ":" + ("00" + (workMinutes % 60).toString()).substr(-2, 2) +
            "-" + ("00" + Math.trunc((workMinutes + duration - 1) / 60).toString()
            ).substr(-2, 2) + ":" + ("00" + ((workMinutes + duration - 1) % 60).toString()).substr(-2, 2)
    );
};

const formatData = (data) => {
    let retData = new Array();
    const lines = data.split("\n");
    lines.map((item) => {
        const day = item.split(" ")[0] - 1;
        const timeRange = item
            .split(" ")[1]
            .split("-")
            .map((time) => time.split(":").map((i) => parseInt(i)));
        const timeRangeFormated = [
            day * 24 * 60 + timeRange[0][0] * 60 + timeRange[0][1],
            day * 24 * 60 + timeRange[1][0] * 60 + timeRange[1][1]
        ];
        retData.push({
            start: timeRangeFormated[0],
            end: timeRangeFormated[1],
            duration: timeRangeFormated[1] - timeRangeFormated[0]
        });
    });
    return retData.sort((a, b) => a.start - b.start);
};

const findTimeSlot = (rawData, duration, config) =>
    new Promise((resolve, reject) => {
        const data = formatData(rawData);

        const busyTimes = (() => {
            let tmp = new Array();
            data.map((item) => {
                for (let count = item.start; count <= item.end; count++)
                    tmp[count] = 1;
            });
            return tmp;
        })();

        for (let workDay = 0; workDay < 5; workDay++) {
            for (
                let workMinutes = config.workStart;
                workMinutes + duration < config.workEnd;
                workMinutes++
            ) {
                const currentTime = workDay * 60 * 24 + workMinutes;
                for (
                    let freeSlot = 0;
                    !busyTimes[currentTime + freeSlot];
                    freeSlot++
                ) {
                    if (freeSlot === duration - 1) {
                        resolve(formatDate(workDay, workMinutes, duration));
                        return;
                    }
                }
            }
        }
        resolve("free slot not found");
        return;
    });

exports.freeTimeSlot = findTimeSlot;
