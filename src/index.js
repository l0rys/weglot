const fs = require("fs");
const { freeTimeSlot } = require("./findTimeSlot");

const workingHours = {
    workStart: 8 * 60, // 08:00
    workEnd: 17 * 60 + 59 // 17:59
};

if (process.argv.slice(2)[0] === "-server") {
    const express = require("express");
    const formidable = require("formidable");
    const app = express();

    app.use((req, res) => {
        if (req.url == "/fileupload") {
            var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                fs.readFile(files.file.path, "utf8", (err, data) => {
                    if (err) console.log(err);
                    else
                        freeTimeSlot(data, 60, workingHours).then(
                            (response) => {
                                res.write(response);
                                res.end();
                            }
                        );
                });
            });
        } else {
            fs.readFile(
                __dirname +
                    "/../reactApp" +
                    (req.url == "/" ? "/index.html" : req.url),
                function (err, data) {
                    if (err) {
                        res.writeHead(404);
                        res.end(JSON.stringify(err));
                        return;
                    }
                    res.writeHead(200);
                    res.end(data);
                }
            );
        }
    });

    app.listen(5000, () => {
        console.log("server started on port 5000");
    });
} else {
    fs.readFile(0, (err, data) => {
        freeTimeSlot(data.toString(), 60, workingHours).then((response) => {
            process.stdout.write(response + "\n");
        });
    });
}
