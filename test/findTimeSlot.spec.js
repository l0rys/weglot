const assert = require("assert");
const fs = require("fs");
const { freeTimeSlot } = require("../src/findTimeSlot");

const filesList = fs.readdirSync("./data/");
const inputList = filesList
    .filter((item) => item.substr(0, 2) === "in")
    .map((item) => fs.readFileSync("./data/" + item).toString());
const outputList = filesList
    .filter((item) => item.substr(0, 2) === "ou")
    .map((item) => fs.readFileSync("./data/" + item).toString());

const workingHours = {
    workStart: 8 * 60, // 08:00
    workEnd: 17 * 60 + 59 // 17:59
};

describe("Find free time slot", () => {
    inputList.forEach((item, index) => {
        it(
            "input" +
                (index + 1) +
                " is rendering the same as output" +
                (index + 1),
            () => {
                freeTimeSlot(item, 60, workingHours).then((response) =>
                    assert.equal(response, outputList[index])
                );
            }
        );
    });
});
